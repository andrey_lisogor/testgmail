package com.andrey.pages;

/**
 * Created by aalis_000 on 29.03.2016.
 */
import static com.codeborne.selenide.Selenide.*;

public class Gmail {

    public static void visit() {
        open("http://gmail.com");
    }

    public static void logIn(String login, String password) {
        $("#Email").setValue(login).pressEnter();
        $("#Passwd").setValue(password).pressEnter();
    }
}