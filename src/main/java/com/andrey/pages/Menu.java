package com.andrey.pages;


import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
/**
 * Created by aalis_000 on 29.03.2016.
 */

public class Menu {

    public static void refresh() {
        $(".asf").click();
    }

    public static void goToInbox() {
        $(By.partialLinkText("Inbox")).click();
    }

    public static void goToSent() {
        $(By.linkText("Sent Mail")).click();
    }
}