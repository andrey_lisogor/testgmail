package com.andrey;

import com.andrey.pages.Gmail;
import com.andrey.pages.Mails;
import com.andrey.pages.Menu;
import com.codeborne.selenide.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;

import static com.andrey.Helpers.getUniqueText;

/**
 * Created by aalis_000 on 29.03.2016.
 */
public class GmailTest {

    @BeforeClass
    public static void setup(){
        Configuration.timeout = 15000;
    }

    private String mailSubject = getUniqueText("tasj");

    @Test
    public void testLoginSendAndSearch() {

        Gmail.visit();

        Gmail.logIn(Conf.email, Conf.password);

        Mails.send(Conf.email, mailSubject);

        Menu.refresh();
        Mails.assertMail(0, mailSubject);

        Menu.goToSent();
        Mails.assertMail(0, mailSubject);

        Menu.goToInbox();
        Mails.searchBySubject(mailSubject);
        Mails.assertMails(mailSubject);
    }

}
